Command line instructions
You can also upload existing files from your computer using the instructions below.
Git global setup

git config --global user.name "zaid alam"
git config --global user.email "zaidimtiaz@gmail.com"

Copy SSH key generated in you local machine(ssh-keygen -t rsa)
Press “Enter” every time till you see it finish
navigate to the .ssh folder( cd ~/.ssh/id_rsa.pub
copy the contents of  vi ~/.ssh/id_rsa.pub (vim for windows users)
paste it in https://gitlab.com/  -> Click Profile image(top right corner) -> Click Preference -> SSH Keys


Make new project in gitlab.com  --> take the key

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:zaidimtiaz/demo123.git
git push -u origin --all
git push -u origin --tags





Create a new repository

git clone git@gitlab.com:zaidimtiaz/demo123.git
cd demo123
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:zaidimtiaz/demo123.git
git add .
git commit -m "Initial commit"
git push -u origin main
